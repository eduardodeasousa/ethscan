<?php

use PDO;

require '../config.php';

$config['startAt'] = (int)89000;
set_time_limit(0);
try {
    $dbh = new PDO("pgsql:dbname=ethscan;host=localhost", 'postgres', 'cp65482jf');
} catch (PDOException  $e) {
    echo "tryPDO";
    print $e->getMessage();
    return;
}

try {
    $sql = "SELECT block_number 
            FROM transactions
            ORDER BY id DESC
            LIMIT 1";
    $statement = $dbh->prepare($sql);
    $statement->execute(array());
    if ($statement->rowCount() == 0) {
        $blockStart = $config['startAt'];
    } else {
        $instance = $_GET['instance'];
        if ($instance != '1' && $instance != '2' && $instance != '3') {
            echo "Define an instance";
            flush();
            ob_flush();
            return;
        }
        $sql = "SELECT block_number 
            FROM transactions
            WHERE cast(block_number as integer) > ?
            AND cast(block_number as integer) < ?
            ORDER BY cast(block_number as integer) DESC
            LIMIT 1";
        $statement = $dbh->prepare($sql);
        $statement->execute(array($config[$instance]['start'], $config[$instance]['end']));
        $result = $statement->fetchAll(PDO::FETCH_ASSOC)[0]['block_number'];
        if ($result + 1 == 1) {
            $blockStart = $config[$instance]['start'];
        } else {
            $blockStart = $result + 1;
        }

    }
    echo "The crawler will start at block: " . $blockStart;
    echo "<br>";
    flush();
    ob_flush();
} catch (Exception $ex) {
    echo "Problem retrieving the last block";
    echo "<br>";
    flush();
    ob_flush();
    return;
}

for ($i = $blockStart; $i < $config[$instance]['end']; $i++) {
    flush();
    ob_flush();
    $query = 'https://api.etherscan.io/api?module=proxy&action=eth_getBlockByNumber&tag=0x'
        . dechex($i) . '&boolean=true&apikey=' . $config[$instance]['key'];

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $query);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    $resultJSON = curl_exec($curl);
    $result = json_decode($resultJSON);
    curl_close($curl);

    if ($result->result->transactions != NULL) {
        echo "Block " . $i . " has " . count($result->result->transactions) . " transaction(s)";
        echo "<br>";
        foreach ($result->result->transactions as $transaction) {
            try {
                $blockhash = $transaction->blockHash;
                $blocknumber = hexdec($transaction->blockNumber);
                $userFrom = $transaction->from;
                $gas = hexdec($transaction->gas);
                $gasPrice = hexdec($transaction->gasPrice);
                $hash = $transaction->hash;
                $input = $transaction->input;
                $nonce = hexdec($transaction->nonce);
                $to = $transaction->to;
                $transactionIndex = $transaction->transactionIndex;
                $value = number_format(hexdec($transaction->value), 0, '.', '');
                $time = date("Y-m-d H:i:s", hexdec($result->result->timestamp));

                $sql = "INSERT INTO transactions (block_hash,block_number,user_from,gas,gas_price,hash,input,nonce,user_to,transaction_index,value,approved,timestamp)
            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
                $statement = $dbh->prepare($sql);
                if (!$statement->execute(array($blockhash, $blocknumber, $userFrom, $gas, $gasPrice, $hash, $input, $nonce, $to, $transactionIndex, $value, 1, $time))) {
                    flush();
                    ob_flush();
                    throw new Exception("Problem inserting on DB");
                }
            } catch (Exception $ex) {
                var_dump(array($blockhash, $blocknumber, $userFrom, $gas, $gasPrice, $hash, $input, $nonce, $to, $transactionIndex, $value, true));
                echo "Error" . $ex . " code: " . $statement->errorCode();
                echo "<br>";
                echo $statement;
                var_dump($statement->errorCode());
                flush();
                ob_flush();
                return;
            }
        }
    } else {
        echo "Block " . $i . " got no transactions";
        echo "<br>";
        flush();
        ob_flush();
    }
}

return;
