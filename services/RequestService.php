<?php

class RequestService extends Service
{
    protected $_QUEUE = array();

    function pushQuery($query)
    {
        array_push($this->_QUEUE, $query . $this->token);
    }

    function executeQuery()
    {

        $query = array_pop($this->_QUEUE);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $query);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $resultJSON = curl_exec($curl);
        $result = json_decode($resultJSON);
        curl_close($curl);
//        if ($result->message =='OK') {
            return $result;
//        }
//        return 'Error '. $result->status. ' Message: '. $result->message;
    }

    function currentToken() {
        if ($this->token == null) {
            return 'Not Found';
        }
        return $this->token;
    }
}
