--
-- 0.0.2
--

BEGIN;

--
--
--

ALTER TABLE transactions
ADD COLUMN timestamp timestamp without time zone;

--
--
--

ALTER TABLE transactions
ALTER COLUMN value 
TYPE varchar(40);

--
--
--

UPDATE versions
    SET current = '0.0.2';

--
--
--

COMMIT;
