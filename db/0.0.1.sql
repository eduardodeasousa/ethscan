--
-- 0.0.1
--

BEGIN;

--
--
--

    CREATE TABLE transactions (
        id serial,
        block_hash varchar(70),
        block_number varchar(50),
        user_from varchar(50),
        gas varchar(30),
        gas_price varchar(30),
        hash varchar(70),
        input text,
        nonce varchar(10),
        user_to varchar(50),
        transaction_index varchar(10),
        value varchar(30),
        approved boolean,
        CONSTRAINT transactions_pkey PRIMARY KEY(id)
    );

--
--
--

CREATE TABLE versions (
    id serial,
    current varchar(10),
    CONSTRAINT versions_pkey PRIMARY KEY(id)
);

--
--
--

INSERT INTO versions
    (current) VALUES ('0.0.1');

--
--
--

COMMIT;


