<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Ethscan UFJF</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="navbar-top-fixed.css" rel="stylesheet">
</head>

<body>

<?php
spl_autoload_register(function ($class_name) {
    include 'services/' . $class_name . '.php';
});
$requests = new RequestService(); ?>
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">Ethscan UFJF</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
            aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="http://www.ufjf.br/pgcc/">PGCC UFJF</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    API
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                </div>
            </li>
        </ul>
        <form class="form-inline mt-2 mt-md-0">
            <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</nav>

<main role="main" class="container">
    <div class="jumbotron">
        <h1>Consultar Saldo</h1>
        <p class="lead">Exemplo de consulta ao saldo Ether de uma unica conta:

            <?php

//            $requests->pushQuery('https://api.etherscan.io/api?module=account&action=balance&address=0xddbd2b932c763ba5b1b7ae3b362eac3e8d40121a&tag=latest&apikey=');
//            $result = $requests->executeQuery();

            //Show Query Result

//            echo $result
            ?></p>
        <a class="btn btn-lg btn-primary" href="/pages/singleBalance.php" role="button">Consultar saldo</a>
    </div>
    <div class="jumbotron">
        <h1>Consultar Saldo</h1>
        <p class="lead">Exemplo de consulta ao saldo Ether de uma unica conta:

            <?php

//            $requests->pushQuery('https://api.etherscan.io/api?module=account&action=balance&address=0xddbd2b932c763ba5b1b7ae3b362eac3e8d40121a&tag=latest&apikey=');
//            $result = $requests->executeQuery();

            //Show Query Result

//            echo $result
            ?></p>
        <a class="btn btn-lg btn-primary" href="/pages/singleBalance.php" role="button">Consultar saldo</a>
    </div>
    <div class="jumbotron">
        <h1>Crawler</h1>
        <p class="lead">Iniciar processo do crawler:

            <?php

            $requests->pushQuery('https://api.etherscan.io/api?module=proxy&action=eth_getBlockByNumber&tag=0x10d4f&boolean=true&apikey=');
            $result = $requests->executeQuery();

            //Show Query Result

            echo $result
            ?></p>
        <a class="btn btn-lg btn-primary" href="#" role="button">Iniciar</a>
    </div>
</main>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>

</body>
</html>
